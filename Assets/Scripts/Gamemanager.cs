﻿////////////////////////////////////////////////////////////////////////////
//
//	Create by: Murtaza Sadikot
//	Date : *Today's date*
//
////////////////////////////////////////////////////////////////////////////
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gamemanager : MonoBehaviour
{
    #region PUBLIC_VARIABLES

    #endregion

    #region PRIVATE_VARIABLES
    private Grid grid;
    private int sizex;
    private int sizey;
    private bool canRun;
    private bool[,] nextCellStates;
    #endregion

    #region UNITY_METHODS

    // Start is called before the first frame update
    IEnumerator Start()
    {
        yield return new WaitForSeconds(0.5f);

        grid = FindObjectOfType<Grid>();
        sizex = grid.m_GridCells.GetLength(0);
        sizey = grid.m_GridCells.GetLength(1);
        nextCellStates = new bool[sizex, sizey];

       // Debug.Log(grid.m_GridCells[0,0].GetComponent<Cell>().m_IsAlive);

        //for (int i = 0; i < sizex; i++)
        //{
        //    for (int j = 0; j < sizey; j++)
        //    {
        //        cells[i, j] = grid.m_GridCells[i, j].GetComponent<Cell>();
        //    }
        //}
    }

    // Update is called once per frame
    void Update()
    {
        if(canRun)
        {
            for (int i = 0; i < sizex; i++)
            {
                for (int j = 0; j < sizey; j++)
                {
                    // Check the cell's current state, and count its living neighbors.
                    bool living = grid.m_GridCells[i, j].GetComponent<Cell>().m_IsAlive;
                    int count = GetLivingNeighbors(i, j);
                    bool result = false;

                    // Apply the rules and set the next state.
                    if (living && count < 2)
                        result = false;
                    if (living && (count == 2 || count == 3))
                        result = true;
                    if (living && count > 3)
                        result = false;
                    if (!living && count == 3)
                        result = true;

                    Debug.Log("next state result : " + result);
                    nextCellStates[i, j] = result;                   
                }
            }
               
            SetNextState();
        }
    }

    #endregion

    #region PRIVATE_METHODS
    private int GetLivingNeighbors(int x, int y)
    {
        int count = 0;

        // Check cell on the right.
        if (x != sizex - 1)
            if (grid.m_GridCells[x + 1, y].GetComponent<Cell>().m_IsAlive)
                count++;

        // Check cell on the bottom right.
        if (x != sizex - 1 && y != sizey - 1)
            if (grid.m_GridCells[x + 1, y + 1].GetComponent<Cell>().m_IsAlive)
                count++;

        // Check cell on the bottom.
        if (y != sizey - 1)
            if (grid.m_GridCells[x, y + 1].GetComponent<Cell>().m_IsAlive)
                count++;

        // Check cell on the bottom left.
        if (x != 0 && y != sizey - 1)
            if (grid.m_GridCells[x - 1, y + 1].GetComponent<Cell>().m_IsAlive)
                count++;

        // Check cell on the left.
        if (x != 0)
            if (grid.m_GridCells[x - 1, y].GetComponent<Cell>().m_IsAlive)
                count++;

        // Check cell on the top left.
        if (x != 0 && y != 0)
            if (grid.m_GridCells[x - 1, y - 1].GetComponent<Cell>().m_IsAlive)
                count++;

        // Check cell on the top.
        if (y != 0)
            if (grid.m_GridCells[x, y - 1].GetComponent<Cell>().m_IsAlive)
                count++;

        // Check cell on the top right.
        if (x != sizex - 1 && y != 0)
            if (grid.m_GridCells[x + 1, y - 1].GetComponent<Cell>().m_IsAlive)
                count++;

        return count;
    }

    private void SetNextState()
    {
        for (int i = 0; i < sizex; i++)
        {
            for (int j = 0; j < sizey; j++)
            { 
                grid.m_GridCells[i, j].GetComponent<Cell>().m_IsAlive = nextCellStates[i, j];
                grid.m_GridCells[i, j].GetComponent<Cell>().material.color = nextCellStates[i,j] == true ? Color.white : Color.black;
            }
        }
    }
    #endregion

    #region PUBLIC_METHODS
    public void StartMutation()
    {
        canRun = true;
    }

    public void Reset()
    {
        canRun = false;
        for (int i = 0; i < sizex; i++)
        {
            for (int j = 0; j < sizey; j++)
            {
                nextCellStates[i,j] = false;
                grid.m_GridCells[i, j].GetComponent<Cell>().material.color = Color.black;
            }
        }
        SetNextState();
    }
    #endregion
}
