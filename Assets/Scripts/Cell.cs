﻿////////////////////////////////////////////////////////////////////////////
//
//	Create by: Murtaza Sadikot
//	Date : *Today's date*
//
////////////////////////////////////////////////////////////////////////////
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cell : MonoBehaviour
{
    #region PUBLIC_VARIABLES
   // [HideInInspector]
    public bool m_IsAlive;
    [HideInInspector]
    public Material material;
    #endregion

    #region PRIVATE_VARIABLES
    #endregion

    #region UNITY_METHODS

    private void Awake()
    {
        material = GetComponent<Renderer>().material;
    }
    private void OnMouseDown()
    {
        Debug.Log("Cell hit");
        m_IsAlive = !m_IsAlive;
        if (m_IsAlive)
            material.color = Color.white;
        else
            material.color = Color.black;
    }

    #endregion

    #region PRIVATE_METHODS

    #endregion

    #region PUBLIC_METHODS

    #endregion
}
