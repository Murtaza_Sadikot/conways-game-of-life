﻿////////////////////////////////////////////////////////////////////////////
//
//	Create by: Murtaza Sadikot
//	Date : *Today's date*
//
////////////////////////////////////////////////////////////////////////////
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    #region PUBLIC_VARIABLES

    #endregion

    #region PRIVATE_VARIABLES
    [SerializeField]
    private Button m_MutateButton;
    [SerializeField]
    private Button m_ResetButton;

    private Gamemanager gamemanager;
	#endregion

	#region UNITY_METHODS

    // Start is called before the first frame update
    void Start()
    {
        gamemanager = FindObjectOfType<Gamemanager>();
        m_MutateButton.onClick.AddListener(delegate {
            OnMutateButtonPressed();
        });

        m_ResetButton.onClick.AddListener(delegate {
            OnResetButtonPressed();
        });
    }

    // Update is called once per frame
    void Update()
    {
        
    }

	#endregion

	#region PRIVATE_METHODS
    private void OnMutateButtonPressed()
    {
        gamemanager.StartMutation();
    }

    private void OnResetButtonPressed()
    {
        gamemanager.Reset();
    }
	#endregion

	#region PUBLIC_METHODS

	#endregion
}
