﻿////////////////////////////////////////////////////////////////////////////
//
//	Create by: Murtaza Sadikot
//	Date : *Today's date*
//
////////////////////////////////////////////////////////////////////////////
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grid : MonoBehaviour
{
    #region PUBLIC_VARIABLES
    public GameObject[,] m_GridCells;
    public int m_GridSize;
    public GameObject m_CellPrefab;
    public Transform m_GridParent;
	#endregion

	#region PRIVATE_VARIABLES

	#endregion

	#region UNITY_METHODS

    // Start is called before the first frame update
    void Start()
    {
        m_GridCells = new GameObject[m_GridSize, m_GridSize];
        GenerateGrid();
    }

    #endregion

    #region PRIVATE_METHODS
    void GenerateGrid()
    {
        for (int i = 0; i < m_GridCells.GetLength(0); i++)
        {
            for (int j = 0; j < m_GridCells.GetLength(1); j++)
            {

                m_GridCells[i, j] = Instantiate(m_CellPrefab, new Vector3(j, 0, i), Quaternion.identity, m_GridParent);
                m_GridCells[i, j].transform.localEulerAngles = new Vector3(90f, 0f, 0f);
            }
        }
    }
    #endregion

    #region PUBLIC_METHODS

    #endregion
}
